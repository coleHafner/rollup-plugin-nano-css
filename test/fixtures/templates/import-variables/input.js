import { nano } from '../../nano-def';
import { Colors } from './colors';

const result = nano.sheet({
	input: {
		border: '1px solid grey',
	},
	button: {
		border: '1px solid red',
		color: Colors.red,
	}
});
const str = result.button;
export default str;

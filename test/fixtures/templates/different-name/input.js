import { nano as anno } from '../../nano-def';

const result = anno.sheet({
	input: {
		border: '1px solid grey',
	},
	button: {
		border: '1px solid red',
		color: 'red',
	}
});
const str = result.button;
export default str;

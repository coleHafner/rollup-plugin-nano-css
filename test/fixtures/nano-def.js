import { create } from 'nano-css';
import { addon as addonRule } from 'nano-css/addon/rule';
import { addon as addonSheet } from "nano-css/addon/sheet";
import { addon as addonStable } from 'nano-css/addon/stable';
import { addon as addonVirtual } from 'nano-css/addon/virtual';
const nano = create();

addonRule(nano);
addonSheet(nano);
addonStable(nano);
addonVirtual(nano);

export { nano };

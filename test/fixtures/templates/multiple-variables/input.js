import { nano } from '../../nano-def';
const inputStyles = {
	border: '1px solid grey',
};
const result = nano.sheet({
	input: inputStyles,
	input2: inputStyles,
	button: {
		border: '1px solid red',
		color: 'red',
	}
});
const str = result.button;
export default str;

import buble from 'rollup-plugin-buble';

var pkg = require('./package.json');

export default {
	input: 'src/index.js',
	plugins: [buble()],
	output: [
		{
			format: 'cjs',
			file: pkg['main']
		},
		{
			format: 'es',
			file: pkg['jsnext:main']
		}
	]
};

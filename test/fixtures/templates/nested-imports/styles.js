import { nano } from '../../nano-def';
import Colors from './Colors';

const metricStyles = {
	color: Colors.$filter_anchor, //This doesn't work, see https://bitbucket.org/TheBosZ/rollup-plugin-nano-css/issues/2/
	'font-size': '1rem',
	'letter-spacing': '.2px',
};

export default nano.sheet({
	icon: metricStyles,
});

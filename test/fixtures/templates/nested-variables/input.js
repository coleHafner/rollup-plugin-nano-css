import { nano } from '../../nano-def';
const nestedStyle = {
	border: '1px solid blue',
};
const inputStyles = {
	border: '1px solid grey',
	button: nestedStyle,
};
const result = nano.sheet({
	input: inputStyles,
	button: {
		border: '1px solid red',
		color: 'red',
	}
});
const str = result.button;
export default str;

import { create } from 'nano-css';
import { addon } from 'nano-css/addon/rule';
import { addon as addon$1 } from 'nano-css/addon/sheet';
import { addon as addon$2 } from 'nano-css/addon/stable';
import { addon as addon$3 } from 'nano-css/addon/virtual';

const nano = create();

addon(nano);
addon$1(nano);
addon$2(nano);
addon$3(nano);

const result = nano.sheet({
	input: {
		border: '1px solid grey',
	},
	button: {
		border: '1px solid red',
		color: 'red',
	}
});

const result2 = nano.rule({
	color: 'blue',
});

var input = {button: result.button, bob: result2};

export default input;

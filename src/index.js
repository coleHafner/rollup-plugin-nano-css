import { createFilter } from 'rollup-pluginutils';
import MagicString from 'magic-string';
import { addon } from 'nano-css/addon/extract';
import { walk } from 'estree-walker';
import { writeFileSync, readFileSync } from 'fs';
import { ensureFileSync } from 'fs-extra';

export default function nanoCSS(opts = {}) {

	const filter = createFilter(opts.include, opts.exclude);

	if (!opts.nano) {
		throw new Error('must specify nano');
	}

	const nano = opts.nano;
	addon(nano);

	function finishedGenerate(bundleOpts, bundle, isWrite) {
		if (opts.outputFile && (typeof isWrite === 'undefined' || isWrite)) {
			ensureFileSync(opts.outputFile);
			writeFileSync(opts.outputFile, nano.raw);
		}
	}

	return {
		name: 'nano-css',

		transform: function (code, id) {
			if (!filter(id)) {
				return null;
			}

			const that = this;

			let canRemoveImport = true;

			const magicString = new MagicString(code);
			const result = that.parse(code);

			let importStart = null, importEnd = null, importName = 'nano', foundImport = false;
			let isNano = false;

			walk(result, {
				enter(node) {
					//Find the import declarations for nano and keep track of it
					const walkThat = this;
					if (!node || !node.type) {
						return;
					}

					if (node.type === 'ImportDeclaration' && node.specifiers && !foundImport) {

						const specs = node.specifiers;
						specs.forEach(spec => {
							if ((spec.imported && spec.imported.name === 'nano') ||
								(spec.local && spec.local.name === 'nano')
							) {
								importStart = node.start;
								importEnd = node.end;
								importName = spec.local.name;
								isNano = true;
								foundImport = true;
							}
						});
					}

					if (node.type === 'VariableDeclaration' && node.declarations && node.declarations.length && !foundImport) {
						//Check to see if this declaration is the require('nano'); we're looking for
						const decs = node.declarations;
						decs.forEach(dec => {
							if (dec.id) {
								//They might use const nano = require('./nano');
								if (
									dec.id.type === 'Identifier' &&
									dec.id.name === 'nano' &&
									!foundImport
								) {
									foundImport = true;
									importStart = node.start;
									importEnd = node.end;
									importName = dec.id.name;
									isNano = true;
									foundImport = true;
								}

								//They've might use: const { nano } = require('./nano');
								if (
									dec.id.type === 'ObjectPattern' &&
									dec.id.properties &&
									dec.id.properties.length &&
									!foundImport
								) {
									const props = dec.id.properties;
									props.forEach(prop => {
										if (prop.key && prop.key.name === 'nano') {
											foundImport = true;
											importStart = node.start;
											importEnd = node.end;
											importName = prop.key.name;
											isNano = true;
											foundImport = true;
										}
									});
								}
							}
						});
					}

					//Find any calls to nano.sheet
					if (node.type === 'CallExpression' && node.callee && node.callee.type === 'MemberExpression') {
						const callee = node.callee;
						if (callee.object.name === importName) {
							//If the call was to something *other* than nano.sheet or nano.rule or nano.put, we can't remove the import
							if (
								callee.property.name !== 'sheet' &&
								callee.property.name !== 'rule' &&
								callee.property.name !== 'put'
							) {
								canRemoveImport = false;
							}

							if (callee.property.name === 'sheet' && node.arguments) {
								const args = node.arguments;
								let x;
								if (args.length) {
									// console.log('found nano, going to require file', id);
									try {
										x = require(id);
										// console.log('----');
										// console.log(x);
										// console.log('====');
										// if (x === false && x === true) {
										// 	that.warn('x === false AND true!', x);
										// }
									} catch (ex) {
										that.warn('Failed to simplify nano call');
										that.warn('Failed to load file: ' + ex.toString());
										// console.error(ex);
										// console.log('------------');
										// console.log(code);
										// console.log('----------');
										// const c = readFileSync(id);
										// console.log(c.toString());
										// console.log('------------');
										// throw ex;
										canRemoveImport = false;
									}

									if (x && canRemoveImport) {


										try {
											const replacedString = x.default ? JSON.stringify(x.default) : JSON.stringify(x);
											if (replacedString.length < 3) {
												that.warn('Replacement string is not long enough, not going to overwrite');
												canRemoveImport = false;
											} else {
												magicString.addSourcemapLocation(0);
												magicString.addSourcemapLocation(code.length);
												magicString.overwrite(0, code.length, replacedString);
												walkThat.skip(); //Finish going through the tree, we're done
											}

										} catch (ex) {
											that.warn('Failed to simplify nano call: ' + ex.toString());
											canRemoveImport = false;
										}
									}
								}
							}
						}
					}
				}
			});
			// console.log('before removing import');
			// console.log('----');
			// console.log(magicString.toString());
			// console.log('------');
			if (isNano && canRemoveImport) {
				// magicString.overwrite(importStart, importEnd, '');
				// console.log('removing import');
				return {
					code: `export default ${magicString.toString()};`,
					map: magicString.generateMap(),
				};
			}

			// console.log('going to write code');
			// console.log('------');
			// console.log(magicString.toString());
			// console.log('------');

			// console.log('Going to return result', r);
			return code;

		},
		ongenerate: finishedGenerate,
		generateBundle: finishedGenerate

	};
}

Support using [Nano CSS](https://github.com/streamich/nano-css) in Rollup.

This plugin is super hacky and may break on your code.

It's most useful when you want to just use `nano.sheet()` and/or `nano.rule()` and extract a regular CSS file.

If you're having troubles with `import`-style imports, you may have to use the CommonJS style of importing (`require`). I'll put up an example eventually of what I mean.

## Installation

```sh
npm i rollup-plugin-nano-css -D
```

## Usage

```js
import { rollup } from 'rollup';
import nanoPlugin from 'rollup-plugin-nano-css';
import nanoObj from './src/nano';

rollup({
	entry: 'main.js',
	plugins: [
		nanoPlugin({
			// Required to be specified
			nano: nanoObj,

			// Undefined by default
			include: '**/*.js',
			exclude: ['**/index.js'],
			outputFile: 'dist/css/gen-styles.css',
		})
	]
});
```

# License

MIT © Nathan Kerr

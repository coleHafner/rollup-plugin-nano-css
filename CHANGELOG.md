# Changelog

## 0.1.2

Recognize `.put()` calls. They kinda almost work the same as `.sheet()`. They're useful to put "reset" or "base" styles in.

## 0.1.1

Properly recognize `.rule()` calls. They work the same as `.sheet()` and so should be allowed.

## 0.1.0

Better tree-shaking by replacing Nano file with result of the `require` call

## 0.0.4

Found a situation where the resulting style object gets tree-shaken to nothing. Workaround to try to prevent that.

## 0.0.3

Initial release
